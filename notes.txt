any thing can be associated with an arrow


a.ass(b)
a  -> b

a.dass(b)
a <-> b

Core.ass(a, b)
a  -> b

Core.dass(a, b)
a <-> b


associations are arbirtrary internally. 
a single join table holds all associations. double arrows are represented by a set of single arrow associations

the public api only allows certain associations. permissions graph starts with no edges. allowed associations are drawn explicitly.

for searching, a node is reachable from another node if there is an arrow pointing from the start node to the destination node

edges are named




objects are typed
objects have denormalized values called properties
objects have normalized arbitrary named values called attributes

emp
emp_attr
erply_emp


employee
^corpdiv
customer
^customertype
location
locationtype
[products]
users
^userroles

^coreres
modules
clients (core clients)





create    insert, option to fail all in a transaction or just keep on truckin'
inform    iodku
delete    delete
alias     secondary key associations (replace into semantics) secondary keys must be unique
fetch     fast, exact, limited
search    slow, fuzzy, featured
assoc     detailed above