var apiDef,fs,grammer,output,parser,Parser;

fs     = require('fs');
Parser = require('jison').Parser;

grammar = fs.readFileSync('trivial.jison',{ encoding: 'ascii' });

parser = new Parser(grammar);

apiDef = fs.readFileSync('api.fdic',{ encoding: 'ascii' });

var symbols = {};
function addSymbol(sym) {
	symbols[sym] = sym;
}

output = parser.parse(apiDef);

console.log(output);