/* description: Parses API Definition files <file>.api */

/* lexical grammar */
%lex

ID		[a-zA-Z][a-zA-Z0-9_]+
NUM		[0-9]+("."[0-9]+)?\b

%%
\s+                   /* skip whitespace   */
"--".+\n              /* skip SQL comments */
"//".+\n              /* skip c++ comments */

"LOOSE"               return 'LOOSE';
"STRICT"              return 'STRICT';

"LIST"                return 'LIST';
"TREE"                return 'TREE';

"PUBLIC"              return 'PUBLIC';
"PRIVATE"             return 'PRIVATE';

"ASSOCIATE"           return 'ASSOCIATE';

"EXTENSION"           return 'EXTENSION';

"USE"                 return 'USE';

":="                  return ':=';
"=="                  return '==';
"=:"                  return '=:';

"->"                  return '->';
"<->"                 return '<->';
"<-"                  return '<-';

"("                   return '(';
")"                   return ')';
","                   return ',';

/* FIELD TYPES */
"BOOL"                return 'BOOL';

"DECIMAL"             return 'DECIMAL';
"DOUBLE"              return 'DOUBLE';
"INT"                 return 'INT';

"CHAR"                return 'CHAR';
"VARCHAR"             return 'VARCHAR';
"STRING"              return 'STRING';

"ASSOC_L"             return 'ASSOC_L';
"ASSOC_LR"            return 'ASSOC_LR';

{ID}                  return 'ID';
{NUM}                 return 'NUM';

<<EOF>>               return 'EOF';

/lex

/* operator associations and precedence */

%left '<-' '<->' '->'

%start apidef

%% /* language grammar */

apidef
	: statements EOF
		{ $$ = $1; return $$; }
	;

statements
	: statements statement
		{ $$ = $1; $$.push($2); }
	|
		{ $$ = []; }
	;

statement
	: type_def
	| using
	| association
	| extension
	| method
	;

type_def
	: type
	| type ":=" field_list "=:"
		{ $$ = $1+'addSymbolFields(['+$3+']);'; }
	;

type
	: itype id
		{ $$ = 'addSymbol('+$2+$1+');'; }
	;
	
itype
	: ktype dtype
		{ $$ = $1+$2; }
	;

ktype
	: LOOSE
		{ $$ = ',ktype.loose'; }
	| STRICT
		{ $$ = ',ktype.strict'; }
	;
	
dtype
	: LIST
		{ $$ = ',dtype.list'; }
	| TREE
		{ $$ = ',dtype.tree'; }
	;

using
	: USE id
		{ $$ = 'useSymbol('+$2+');'; }
	;
	
association
	: ASSOCIATE ":=" assoc_list "=:"
		{ $$ = 'allowAssociations(['+$3+']);'; }
	;

assoc_list
	: assoc_list "," assoc
		{ $$ = $1+','+$3 }
	| assoc
	;

assoc
	: id "->" id
		{ $$ = '['+$1+','+$3+']'; }
	| id "<->" id
		{ $$ = '['+$1+','+$3+'],['+$3+','+$1+']'; }
	| id "<-" id
		{ $$ = '['+$3+','+$1+']'; }
	;

extension
	: EXTENSION id ":=" field_list "=:"
		{ $$ = 'extendSymbol('+$2+',['+$4+']);'; }
	;

field_list
	: field_list "," field
		{ $$ = $1+','+$3; }
	| field
	;

field
	: id ftype
		{ $$ = '['+$1+$2+']'; }
	| id ftype "(" num_list ")"
		{ $$ = '['+$1+','+$2+','+$4+']'; }
	| id atype "(" id ")"
		{ $$ = '['+$1+','+$2+','+$4+']'; }
	;

ftype
	: BOOL
		{ $$ = 'ftype.bool'; }
	| DECIMAL
		{ $$ = 'ftype.decimal'; }
	| DOUBLE
		{ $$ = 'ftype.double'; }
	| INT
		{ $$ = 'ftype.int'; }
	| CHAR
		{ $$ = 'ftype.char'; }
	| STRING
		{ $$ = 'ftype.string'; }
	| VARCHAR
		{ $$ = 'ftype.varchar'; }
	;
	
atype
	: ASSOC_L
		{ $$ = 'ftype.assoc_l' }
	| ASSOC_LR
		{ $$ = 'ftype.assoc_lr' }
	;

method
	: mtype m_list
		{ $$ = 'registerMethod(['+$2+']'+$1+');'; }
	;

mtype
	: PUBLIC
		{ $$ = ',mtype.public'; }
	| PRIVATE
		{ $$ = ',mtype.private'; }
	;
	
m_list
	: m_list "," id
		{ $$ = $1+','+$3; }
	| id
	;
	
id
	: ID
		{ $$ = '"'+yytext+'"' }
	;

num_list
	: num_list "," num
		{ $$ = $1+','+Number(yytext); }
	| num
	;
	
num
	: NUM
		{ $$ = Number(yytext); }
	;
	
