/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex

%%
\s+                   /* skip whitespace */

("PUBLIC"|"PRIVATE")  return 'FN_SCOPE_TYPE';
("LOOSE"|"STRICT")    return 'IF_KEY_TYPE';
("LIST"|"TREE")       return 'IF_DATA_TYPE';
"ASSOCIATE"           return 'ASSOC';

":="                  return ':=';
"=="                  return '==';
"=:"                  return '=:';

"->"                  return '->';
"<->"                 return '<->';
"<-"                  return '<-';

"+"                   return '+';
"-"                   return '-';

\w+                   return 'WORD';
[0-9]+("."[0-9]+)?\b  return 'NUMBER';

<<EOF>>               return 'EOF';

/lex

/* operator associations and precedence */

%left '<-' '<->' '->'
%right '+' '-'

%start apidef

%% /* language grammar */

apidef
	: statements EOF
		{ $$ = $1; return $$; }
	;

statements
    : statements statement
        { $$ = $1; $$.push(2); }
	|
		{ $$ = []; }
    ;

statement
	: interface
	| method
	| association
	| extension
	;
	
interface
	: interface_declaration ':=' fields '==' methods '=:'
		{  }
	;

interface_declaration
	: IF_KEY_TYPE IF_DATA_TYPE WORD
		{  }
	;
	
e
// 	: IF_KEY_TYPE IF_DATA_TYPE e
// 		{$$ = create$2Type($3,$1);}
// 	| FN_SCOPE_TYPE e
// 		{$$ = define$1FN($2);}
// 	| ASSOC ':=' e '=:'
// 		{$$ = $3}
// // 	| e '->' e
// // 		{$$ = allowLAssoc($1,$3);}
// // 	| e '<->' e
// // 		{$$ = allowLRAssoc($1,$3);}
// // 	| e '<-' e
// // 		{$$ = allowLAssoc($3,$1);}
	: '+' e
		{$$ = addMethod(this,'$2','public');}
	| '-' e
		{$$ = addMethod(this,'$2','private');}	
	| WORD
		{$$ = String(yytext);}
	| NUMBER
		{$$ = Number(yytext);}
	;
	
	
	
	