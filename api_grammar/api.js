it = {};

it.list = function() { return 'list'; };
it.tree = function() { return 'tree'; };

kt = {};
kt.loose  = function() { return 'loose'; };
kt.strict = function() { return 'strict'; };

t = {};

t.bool = function() { return 'BOOLEAN'; };

t.date = function() { return 'DATE'; };

t.string = function() { return 'VARCHAR'; };
t.string.n = 40;

IF = {};

IF.employee = {
	type: it.list,
	keyt: kt.strict,
	keys: ['hris','payroll','pos'],
	flds: [
		['FirstName',t.string],
		['LastName',t.string],
		['BirthDate',t.date],
		['HireDate',t.date],
		['TermDate',t.date],
		['Active',t.bool],
		['DivisionID',t.assoc,'division'],
		['LocationID',t.assoc,'location']
	],
	pufn: [
		
	],
	prfn: [
		
	]
}
