# LOOSE LEAF TEA #

So that we can sit back and drink good tea.

## Project Summary ##

The goal of the Loose Leaf Tea project is to provide a powerful toolchain for
rapid, stable, and scalable prototyping and deployment of web/SaaS style
applications.

Ease of development (including maintenance) and minimal interference with 
developers' ability to exercise professional judgement in solving their 
particular problem are key factors in project design decisions. Rigid rules 
about what can and cannot be done should be viewed with suspicion.



## OOLONG ##

A flexible stream/event driven server engine.

Including node modules directly the repository is unnecessary when all 
developers work on a sane platform, and their exclusion greatly simplifies the 
commit record. To install/update the node modules needed by the project, run
`npm install` in the `oolong` folder, or execute `sync.sh` to ensure that 
dependencies are up to date, and also run utility checks for db versions, etc.
