var fn;

fn = {};

fn.config = {
	basepath: path.join(__dirname,'../')
};

// ===== type conversions ===== /
fn.array = function(a) {
	if (!fn.isdef(a)) return [];
	
	return fn.isarray(a) ? a : [a];
};

fn.fixed = function(f,n) {
	return fn.float(f).toFixed(n||2);
}

fn.float = function(f,b) {
	var ff;
	ff = parseFloat(f,b||10);
	return isFinite(ff) ? ff : 0;
}

fn.int = function(n,b) {
	return parseInt(n,b||10)|0;
};


// ===== type signature checks ===== //
fn.isdef = function(v) {
	return typeof v !== 'undefined';
}

fn.isarray = function(a) {
	return a instanceof Array;
}

fn.isbool = function(b) {
	return typeof b === 'boolean';
}

fn.isfn = function(f) {
	return typeof f === 'function';
}

fn.isnum = function(n) {
	return typeof n === 'number';
}

fn.isobj = function(o) {
	return typeof o === 'object';
}

fn.isstring = function(s) {
	return typeof s === 'string';
}


// ===== general functions ===== //
fn.extract = function(o,propA) {
	propA = fn.array(propA);
	
	var out,withProp;
	
	out = {};
	
	withProp = function(prop) {
		prop = array(prop);
		
		if (prop[0]) {
			if (fn[prop[1]]) {
				out[prop[0]] = fn[prop[1]](o[prop[0]]);
			}
			else if (fn.isdef(prop[2])) {
				out[prop[0]] = o[prop[0]] || prop[2];
			}
			else {
				out[prop[0]] = o[prop[0]];
			}
		}
	};
	
	propA.forEach(withProp);
	
	return out;
};

// export the function properties of an object into the global namespace
fn.globalize = function(o) {
	var ifFn;
	
	ifFn = function(prop,name) {
		if (fn.isfn(prop)) {
			global[name] = o[name];
		}
	}
	
	_(o).each(ifFn);
};

fn.orin = function(e,a) {
	a = array(a);
	
	for(var i=0;i<a.length;i++)
		if (e === a[i])
			return true;
	
	return false;
}

fn.tryRequire = function(filepath,errval) {
	var fullpath;
	
	fullpath = path.join(fn.config.basepath,filepath);
	
	try {
		return require(fullpath);
	}
	catch(err) {
		log.warn('Could not require %s',fullpath);
		return errval;
	}
};

module.exports = fn;