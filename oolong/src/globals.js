// the presence of these global includes can be relied on in any file loaded
// by a server within oolong

// external includes
global.$          =
global.jQuery     = require('jquery');
global._          =
global.underscore = require('underscore');
global.clic       = require('cli-color');
global.fs         = require('fs');
global.io         = require('socket.io');
global.moment     = require('moment');
global.path       = require('path');
global.pgsql      = require('pg');
global.util       = require('util');

// internal includes
global.fn         = require('./fn');
global.log        = require('./log');
global.pgpool     = require('./pgpool');