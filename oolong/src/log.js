// logging module to simplify getting useful output to the console,
// as well as ensuring that it is written to file or a db log table,
// depending on the configuration options set
// TODO: add config for logging destinations (stdout/stderr, files, DBs)
var log;

log = function() {
	return console.log.apply(console,arguments);
};

log.config = {
	stdio  : true,
	errfile: false,
	logfile: false,
	logtodb: false,
	dbpool : false
};

log.err = function() {
	return console.error.apply(console,arguments);
};

// colored text for standard status messages
log.T = {
	exit: clic.magenta('EXIT'),
	fail: clic.red('FAIL'),
	info: clic.cyan('INFO'),
	ok  : clic.cyan(' OK '),
	pass: clic.green('PASS'),
	warn: clic.yellow('WARN')
};
// stream to use for standard status messages
log.S = {
	exit: 'log',
	fail: 'error',
	info: 'log',
	ok  : 'log',
	pass: 'log',
	warn: 'error'
};

['exit','fail','info','ok','pass','warn'].forEach(function(msg) {
	log[msg] = function() {
		return log.status(log.S[msg],log.T[msg],util.format.apply(util,arguments));
	};
});

log.status = function(stream,status,text) {
	return console[stream]('[%s] %s',status,text);
};

module.exports = log;