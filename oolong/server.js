require('./src/globals');

log.info('Starting Oolong');

fn.globalize(fn);

global.config = tryRequire('./config/config.global.json');

if (!config) return log.exit('Aborting: No Config');

// extend config with local settings, if any
config = _(config).extend(tryRequire('./config/config.json',{}));

log.exit('Done.');